package utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.OutputStream;

public class StreamUtils {
    public static void closeStreamSafely(BufferedReader stream){
        try{
            if( stream == null ) return;
            stream.close();
        }catch ( Exception ignored ){}
    }

    public static void closeStreamSafely(BufferedWriter stream){
        try{
            if( stream == null ) return;
            stream.close();
        }catch ( Exception ignored ){}
    }
}
