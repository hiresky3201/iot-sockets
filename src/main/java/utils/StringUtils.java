package utils;

public class StringUtils {
    public static String ensureString( String value, String fallback ){
        if( isEmpty( value ) ) return fallback;
        return value;
    }

    public static boolean isEmpty( String value ){
        return value == null || value.length() == 0;
    }
}
