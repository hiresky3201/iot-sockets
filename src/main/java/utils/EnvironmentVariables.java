package utils;

public class EnvironmentVariables {
    public static int PORT;

    static{
        try{
            PORT = Integer.parseInt( System.getenv( "PORT" ) );
        }catch ( Exception ex ){
            ex.printStackTrace();
        }
    }
}
