package entry;

import commands.Command;
import utils.StringUtils;

public class Entry {
    public static void main(String[] args){
        String command = StringUtils.ensureString( System.getenv("COMMAND"), "WSServer" );

        try {
            Command c = (Command) Class.forName( String.format( "commands.%s", command ) ).getConstructor().newInstance();
            c.execute();
        }catch ( Exception e ){
            e.printStackTrace();
            System.exit( 1 );
        }
    }
}
