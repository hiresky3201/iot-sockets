package commands;

import org.apache.log4j.Logger;
import utils.EnvironmentVariables;
import utils.StreamUtils;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.stream.Collectors;

public class SServer implements Command{
    private static final Logger logger = Logger.getLogger( SServer.class );
    private ServerSocket server;

    /**
     * Inicializa este servidor de sockets
     * Por default el address es loopback
     */
    public void execute() {
        try{
            server = new ServerSocket();
            server.bind( new InetSocketAddress( "0.0.0.0", EnvironmentVariables.PORT ));
            logger.debug( String.format( "Socket server - inicializado %s:%s", server.getInetAddress(), EnvironmentVariables.PORT) );

            new Thread( this::acceptClients ).start();
        }catch ( Exception e ){
            e.printStackTrace();
        }
    }

    /**
     * Acepta nuevos clientes, esperando una conexion
     * Al hacer una nueva conexion, se crea un hilo que maneja los mensajes que se reciben
     */
    public void acceptClients(){
        try {
            while ( true ){
                Socket client = server.accept();
                logger.debug( "Nuevo cliente conectado" );

                new Thread(() -> acceptClient( client ) ).start();
            }
        }catch ( Exception e ){
            e.printStackTrace();
        }
    }

    /**
     * Encargado de recibir a un socket, y estar escuchando por todos los
     * mensajes que se reciban de el. Al recibir un nuevo mensaje, se regresa al cliente
     * como confirmacion de que se recibio.
     *
     * @param socket El socket conectado
     */
    public void acceptClient(Socket socket){
        BufferedReader is = null;
        BufferedWriter os = null;

        try{
            is = new BufferedReader( new InputStreamReader( socket.getInputStream() ) );
            os = new BufferedWriter( new OutputStreamWriter( socket.getOutputStream() ) );

            logger.debug( String.format( "Conexion establecida correctamente con %s", socket.getInetAddress() ) );

            while( !socket.isClosed() ){
                if( !is.ready() ) continue;
                String o = is.readLine();

                logger.debug(String.format("[%s] - Se recibio un mensaje: %s", socket.getInetAddress(), o ));
                logger.debug( String.format( "[%s] - Mandando de vuelta el mensaje", socket.getInetAddress() ) );

                os.write( o + System.lineSeparator() );
                os.flush();

                logger.debug( String.format( "[%s] - Mensaje enviado correctamente", socket.getInetAddress() ) );
            }
        }catch ( Exception e ){
            e.printStackTrace();

            StreamUtils.closeStreamSafely( is );
            StreamUtils.closeStreamSafely( os );
        }
    }
}
