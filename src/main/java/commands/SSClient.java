package commands;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Scanner;

public class SSClient implements Command {
    @Override
    public void execute() {
        try{
            int port = 4045;

            Socket server = new Socket();
            server.connect( new InetSocketAddress( "buzzly.art", port ) );

            Scanner scanner = new Scanner( System.in );

            BufferedReader reader = new BufferedReader( new InputStreamReader( server.getInputStream() ) );
            BufferedWriter writer = new BufferedWriter( new OutputStreamWriter( server.getOutputStream() ) );

            System.out.println( "Connected to server on port " + port );

            while( true ){
                String line = scanner.nextLine() + System.lineSeparator();
                System.out.println("Sending line..");

                writer.write( line );
                writer.flush();

                System.out.println("Line sent, reading line..");
                System.out.println("Server Data: " + reader.readLine());
            }
        }catch ( Exception e ){
            e.printStackTrace();
        }
    }
}
