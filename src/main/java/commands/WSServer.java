package commands;

import org.apache.log4j.Logger;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import utils.EnvironmentVariables;

import java.net.InetSocketAddress;

public class WSServer extends WebSocketServer implements Command{
    private static final Logger logger = Logger.getLogger( WSServer.class );
    private static WSServer instance;

    public WSServer(){}
    public WSServer(InetSocketAddress address){ super( address ); }

    public void execute() {
        instance = new WSServer( new InetSocketAddress( "0.0.0.0", EnvironmentVariables.PORT) );
        instance.start();
    }

    public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
        logger.debug( String.format( "Cliente conectado - %s", instance.getAddress() ) );
        webSocket.send( "Te conectaste correctamente al servidor" );
    }

    public void onClose(WebSocket webSocket, int i, String s, boolean b) {
        logger.debug( String.format( "Cliente desconectado - %s", instance.getAddress() ) );
    }

    public void onMessage(WebSocket webSocket, String s) {
        webSocket.send( String.format( "Mensaje Respuesta: %s", s ) );
    }

    public void onError(WebSocket webSocket, Exception e) {
        logger.error( String.format( "Error de conexion - %s", instance.getAddress() ) );
        e.printStackTrace();
    }

    public void onStart() {
        logger.debug( String.format( "Websocket Server inicializado - %s", instance.getAddress() ) );
    }
}
